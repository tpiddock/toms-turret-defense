using UnityEngine;
using System.Collections;

public class Input_Handler : MonoBehaviour
{
	
	private Auto_Turret selectedTurret;
	private Vector3 defaultCameraPosition;
	private Vector3 desiredCameraPosition;
	private Vector3 worldCameraPosition;
	
	public Transform EnemyCube;
	
	// Use this for initialization
	void Start ()
	{
		defaultCameraPosition = Camera.main.transform.position;
		worldCameraPosition = Camera.main.transform.position;
		desiredCameraPosition = worldCameraPosition;
	}
	
	// Update is called once per frame
	void Update ()
	{	

		// Turret Selection Camera
		if (Input.GetMouseButtonDown (0)) {
			UpdateTurretSelection ();
		}
		// Reset Camera
		if (Input.GetMouseButtonDown (1)) {
			desiredCameraPosition = defaultCameraPosition;
		}
		// Adjust Time Scale with mouse scrollwheel
		if (Input.GetAxis ("Mouse ScrollWheel") != 0) {
			UpdateTimeScale ();
		}
		
		if (Input.GetKeyUp(KeyCode.A)){
			
			Instantiate(this.EnemyCube ,new Vector3(30f,3f,30f),new Quaternion(0f,0f,0f,0f));
		}
		
		// Lerp Camera
		if (desiredCameraPosition != Camera.main.transform.position) {
			Camera.main.transform.position = Vector3.Lerp (Camera.main.transform.position, desiredCameraPosition, Time.deltaTime * 5f);
		}		
	}
	
	void UpdateTimeScale ()
	{
		Time.timeScale += Input.GetAxis ("Mouse ScrollWheel");
		Time.timeScale = Mathf.Clamp ( Time.timeScale,0f,1.5f);
		Debug.Log (Time.timeScale);
		
	}
	
	void UpdateTurretSelection ()
	{
		RaycastHit hit = new RaycastHit (); // will contain info on what the raycast hit
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition); // get our ray from camera to mouse position
		if (Physics.Raycast (ray, out hit, 100.0f)) { // send a raycast with our ray that is 100.0f units long. Function returns boolean, raycast hit info stored on hit
			if (hit.transform.tag == "Turret") { // hit.transform is the transform of the object that the raycast hit
				// do whatever here
				desiredCameraPosition = new Vector3 (hit.transform.position.x, hit.transform.position.y + 3f, hit.transform.position.z - 10f);
					
				Auto_Turret selectedTurret = hit.transform.GetComponent (typeof(Auto_Turret)) as Auto_Turret;
				selectedTurret.SendMessage ("UpgradeTurret", Auto_Turret.TurretUpgrade.Green, SendMessageOptions.DontRequireReceiver);
			}
		}
	}
}
