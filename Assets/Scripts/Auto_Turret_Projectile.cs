using UnityEngine;
using System.Collections;

[AddComponentMenu("Projectiles/Auto Turret Projectile")]
[RequireComponent (typeof(CapsuleCollider))]
public class Auto_Turret_Projectile : MonoBehaviour
{
	
	public float speed = 100;
	public float range = 50;
	public float damage = 10;
	private float distanceTravelled;
	private bool projectileConsumed;
	
	// Update is called once per frame
	void Update ()
	{
		UpdatePosition ();
	}
	
	void UpdatePosition ()
	{
		transform.Translate (Vector3.forward * Time.deltaTime * speed);
		distanceTravelled += Time.deltaTime * speed;
		if (distanceTravelled >= range) {
			Destroy (gameObject);	
		}
		
	}
	
	void OnTriggerEnter (Collider objCollider)
	{
		if (!projectileConsumed) { // This boolean prevents the bullet telling other hit objects to apply damage
			if (objCollider.gameObject.tag == "Enemy") {
				objCollider.gameObject.SendMessage ("ApplyDamage", damage, SendMessageOptions.DontRequireReceiver);
				projectileConsumed = true;
				Explode ();
			}		
		}
	}
	
	void Explode ()
	{
		//Insert effect
		Destroy (gameObject);
	}

}
