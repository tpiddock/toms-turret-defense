using System.Collections;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AStarNode))]
public class AStarNodeEditor : Editor
{
	private Color nodeColor = new Color(0f,0f,0.4f,0.2f) ;
	public void OnSceneGUI ()
	{
		AStarNode myTarget = (AStarNode)target;
		foreach (AStarNode neighbour in myTarget.neighbours) {
			if (neighbour) {
				Handles.DrawLine (myTarget.collider.bounds.center, neighbour.collider.bounds.center);
				Handles.CubeCap (0, neighbour.collider.bounds.center,
        					neighbour.transform.rotation, 0.5f);
			}
		}
		Handles.color = nodeColor;
		Handles.CubeCap (0, myTarget.transform.position, 
        					myTarget.transform.rotation, 2f);
       
	}
}