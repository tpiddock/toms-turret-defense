using System;
using UnityEngine;
using System.Collections;

public class AStarNode : MonoBehaviour {
	
	public int fValue = 0;
	public int gValue = 0; 
	public int hValue = 0;
	
	public int cost;
	
	public bool visited = false; 
	public bool closed = false;
	
	public bool occupied = false;
	
	public bool start = false;
	public bool goal = false;
		
	public AStarNode parent = null;
	
	public AStarNode[] neighbours;
			
    public void OnDrawGizmos () {
		Vector3 gizmoLocation = transform.position;
		Gizmos.DrawIcon (gizmoLocation, "AStarGizmoIcon.tif", true);
    }
	
	public void Awake(){
		
	}
	
	public void GetNeighbouringNodes(){
		
	}
		
}
