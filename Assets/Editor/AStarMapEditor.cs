using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(AStarMap))]
public class AStarMapEditor : Editor
{
	AStarMap aStarMap;
	public float nodeWidth = 5f;
	
	public bool diagonalNavigation = false;
	
	public void OnEnable ()
	{
		aStarMap = (AStarMap)target;
		// Setup the SerializedProperties
		//InstantiateNodes ();
	}

	public override void OnInspectorGUI ()
	{
		GUILayout.BeginHorizontal ();
		GUILayout.Toggle(diagonalNavigation,"Use diagonal navigation");
		GUILayout.EndHorizontal ();
		
		GUILayout.BeginHorizontal ();
		if (GUILayout.Button ("Reinstantiate Nodes")) {
			InstantiateNodes ();
		}
			
		GUILayout.EndHorizontal ();
	}
	
	public void InstantiateNodes ()
	{
		AStarNode[] currentNodes = (AStarNode[]) FindObjectsOfType (typeof(AStarNode));// AStarNode[];
        foreach(AStarNode node in currentNodes ) {
            DestroyImmediate (node.gameObject);
        }
		
		for (int i = 0; i < (aStarMap.collider.bounds.size.x/nodeWidth)+1; i++) {
			for (int j = 0; j < (aStarMap.collider.bounds.size.z/nodeWidth)+1; j++) {	
				Transform tmpAstarNode = Instantiate (aStarMap.nodePrefab, new Vector3 (i * nodeWidth, 0f, j * nodeWidth), Quaternion.identity) as Transform;
				tmpAstarNode.parent = aStarMap.transform;
				tmpAstarNode.localPosition = new Vector3 (i * nodeWidth, 0f, j * nodeWidth);
				tmpAstarNode.localRotation = aStarMap.transform.localRotation;
				tmpAstarNode.name = "Node (" + i + "," + j + ")";
				
			}
		}
	}
}
