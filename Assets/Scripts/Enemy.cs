using UnityEngine;
using System.Collections;

[AddComponentMenu("Enemies/Evil Cube")]

public class Enemy : MonoBehaviour
{

	public float health = 100;
	public float speed = 5;
	public Transform enemyObject ;
	public Transform deathParticle;
	private Transform playerBase;
	
	void Awake ()
	{
		
		playerBase = GameObject.FindGameObjectWithTag ("Player Base").transform;
	}
	
	// Update is called once per frame
	void Update ()
	{
		UpdateTargetBase ();
		UpdatePosition ();
	}
	
	void UpdatePosition ()
	{
		transform.LookAt (playerBase, transform.up);
		transform.Translate (Vector3.forward * Time.deltaTime * speed);
		Debug.DrawLine (transform.position, playerBase.position, Color.cyan);
	}
	
	void UpdateTargetBase ()
	{
		
		playerBase = GameObject.FindGameObjectWithTag ("Player Base").transform;
	}
	
	// Called by SendMessage from projectiles
	void ApplyDamage (float damageAmount)
	{
		health -= damageAmount;
		if (health <= 0) {
			Explode ();
		}
	}
	
	void Explode ()
	{
		audio.Play ();
		//fancy effect
		transform.tag = "Dying Enemy";
		enemyObject.gameObject.animation.CrossFade ("Enemy Cube Death");
		Destroy (this.gameObject, 1f);
		//Instantiate(deathParticle,transform.position,transform.rotation);
	}
}
