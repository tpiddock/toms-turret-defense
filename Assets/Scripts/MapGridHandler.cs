using UnityEngine;
using System.Collections;

public class MapGridHandler : MonoBehaviour {
	
	public class GridNode : MonoBehaviour {
			
		public GridNode[] connectedNodes;
		public bool isClosed = false;
		public Vector3 position;
		public int xPos;
		public int yPos;
	}
	
	
	public GridNode[] nodes;
	private Transform myTerrain;
	
	private int mapWidth;
	private int mapHeight;
	
	// Use this for initialization
	void Start () {
			
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
